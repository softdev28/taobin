import 'package:taobinfly/taobinfly.dart' as taobinfly;
import "dart:io";

class Taobinfly {
  String category = "";
  String menu = "";
  String Option = "";
  late String Blended, Sweetness, ExtraTopping, Straw, Lid;
  String MenuName = "";
  late int Price;
  bool isCoffee = false;
  bool isProteinShake = false;

  void showWelcome() {
    print("Choose category:");
    print("1.Hot Coffee");
    print("2.Iced Coffee");
    print("3.Hot Tea");
    print("4.Iced Tea");
    print("5.Hot Milk");
    print("6.Iced Milk");
    print("7.Soda");
    print("8.Protein Shakes");
    print("9.Fruity Drinks");
  }

  void inputCategory() {
    category = stdin.readLineSync()!;
  }

  void process() {
    switch (category) {
      case "1":
        showHotCoffeeMenu();
        inputMenu();
        Option = "1";
        isCoffee = true;
        showOption();
        break;
      case "2":
        Option = "2";
        isCoffee = true;
        showIcedCoffeeMenu();
        inputMenu();
        showOption();
        break;
      case "3":
        Option = "1";
        showHotTeaMenu();
        inputMenu();
        showOption();
        break;
      case "4":
        Option = "2";
        showIcedTeaMenu();
        inputMenu();
        showOption();
        break;
      case "5":
        Option = "1";
        showHotMilkMenu();
        inputMenu();
        showOption();
        break;
      case "6":
        Option = "2";
        showIcedMilkMenu();
        inputMenu();
        showOption();
        break;
      case "7":
        Option = "2";
        showSodaMenu();
        inputMenu();
        showOption();
        break;
      case "8":
        Option = "2";
        showProteinShakesMenu();
        inputMenu();
        showOption();
        break;
      case "9":
        Option = "2";
        showFruityDrinksMenu();
        inputMenu();
        showOption();
        break;

      default:
        print("Exit program.");
        break;
    }
  }

  void inputMenu() {
    menu = stdin.readLineSync()!;
    switch (category) {
      case "1":
        Price = 35;
        MenuName = getHotCoffeeName();
        break;
      case "2":
        Price = 45;
        MenuName = getIcedCoffeeName();
        break;
      case "3":
        Price = 35;
        MenuName = getHotTeaName();
        break;
      case "4":
        Price = 40;
        MenuName = getIcedTeaName();
        break;
      case "5":
        Price = 35;
        MenuName = getHotMilkName();
        break;
      case "6":
        Price = 40;
        MenuName = getIcedMilkName();
        break;
      case "7":
        Price = 30;
        MenuName = getSodaName();
        break;
      case "8":
        isProteinShake = true;
        Price = 50;
        MenuName = getProteinShakesName();
        break;
      case "9": //Fruity Drinks
        Price = 25;
        MenuName = getFruityDrinksName();
        break;
    }
  }

  String getHotCoffeeName() {
    switch (menu) {
      case "1":
        return "Espresso";
      case "2":
        return "Double Espresso";
      case "3":
        return "Hot Americano";
      case "4":
        return "Hot Cafe Latte";
      case "5":
        return "Hot Cappuccino";
      case "6":
        return "Hot Mocha";
      case "7":
        return "Hot Caramel Latte";
      case "8":
        return "Hot Taiwanese Tea Cafe Latte";
      case "9":
        return "Hot Matcha Latte";
      case "10":
        return "Hot Kokuto Cafe Latte";
      case "11":
        return "Hot Thai Tea Cafe Latte";
      case "12":
        return "Hot Lychee Americano";
    }
    return "";
  }

  String getIcedCoffeeName() {
    switch (menu) {
      case "1":
        return "Dirty";
      case "2":
        return "Iced Espresso";
      case "3":
        return "Iced Americano";
      case "4":
        return "Iced Cafe Latte";
      case "5":
        return "Iced Cappuccino";
      case "6":
        return "Iced Mocha";
      case "7":
        return "Iced Caramel Cafe Latte";
      case "8":
        return "Iced Matcha Cafe Latte";
      case "9":
        return "Iced Taiwanese Tea Cafe Latte";
      case "10":
        return "Iced Kokuto Cafe Latte";
      case "11":
        return "Iced Thai Tea Cafe Latte";
      case "12":
        return "Iced Lychee Americano";
      case "13":
        return "Iced Cannabis Americano";
    }
    return "";
  }

  String getHotTeaName() {
    switch (menu) {
      case "1":
        return "Hot Chrysanthemum Tea";
      case "2":
        return "Hot Thai Milk Tea";
      case "3":
        return "Hot Taiwanese Tea";
      case "4":
        return "Hot Matcha Latte";
      case "5":
        return "Hot Black Tea";
      case "6":
        return "Hot Kokuto Tea";
      case "7":
        return "Hot Lime Tea";
      case "8":
        return "Hot Lychee Tea";
      case "9":
        return "Hot Strawberry Tea";
      case "10":
        return "Hot Blueberry Tea";
    }
    return "";
  }

  String getIcedTeaName() {
    switch (menu) {
      case "1":
        return "Iced Chrysanthemum Tea";
      case "2":
        return "Iced Thai Milk Tea";
      case "3":
        return "Iced Taiwanese Milk Tea";
      case "4":
        return "Iced Matcha Latte";
      case "5":
        return "Iced Tea";
      case "6":
        return "Iced Kokuto Tea";
      case "7":
        return "Iced Limeade Tea";
      case "8":
        return "Iced Lychee Tea";
      case "9":
        return "Iced Strawberry Tea";
      case "10":
        return "Iced Blueberry Tea";
    }
    return "";
  }

  String getHotMilkName() {
    switch (menu) {
      case "1":
        return "Hot Caramel Milk";
      case "2":
        return "Hot Kokuto Milk";
      case "3":
        return "Hot Cocoa";
      case "4":
        return "Hot Caramel Cocoa";
      case "5":
        return "Hot Milk";
    }
    return "";
  }

  String getIcedMilkName() {
    switch (menu) {
      case "1":
        return "Iced Caramel Milk";
      case "2":
        return "Iced Kokuto Milk";
      case "3":
        return "Iced Cocoa";
      case "4":
        return "Iced Caramel Cocoa";
      case "5":
        return "Iced Pink Milk";
    }
    return "";
  }

  String getSodaName() {
    switch (menu) {
      case "1":
        return "Pepsi";
      case "2":
        return "Iced Limenade Soda";
      case "3":
        return "Iced Lychee Soda";
      case "4":
        return "Iced Strawberry Soda";
      case "5":
        return "Iced Cannabis Soda";
      case "6":
        return "Iced Plum Soda";
      case "7":
        return "Iced Ginger Soda";
      case "8":
        return "Iced Blueberry Soda";
      case "9":
        return "Iced Sala Soda";
      case "10":
        return "Iced Lime Sala Soda";
    }
    return "";
  }

  String getProteinShakesName() {
    switch (menu) {
      case "1":
        return "Matcha Protein Shake";
      case "2":
        return "Chocolate Protein Shake";
      case "3":
        return "Strawberry Protein Shake";
      case "4":
        isCoffee = true;
        return "Espresso Protein Shake";
      case "5":
        return "Thai Tea Protein Shake";
      case "6":
        return "Brown Sugar Protein Shake";
      case "7":
        return "Taiwanese Tea Protein Shake";
      case "8":
        return "Caramel Protein Shake";
      case "9":
        return "Plain Protein Shake";
      case "10":
        return "Milk Shake";
    }
    return "";
  }

  String getFruityDrinksName() {
    switch (menu) {
      case "1":
        Option = "1";
        return "Hot Limeade";
      case "2":
        return "Iced Limeade";
      case "3":
        return "Iced Lychee";
      case "4":
        return "Iced Strawberry";
      case "5":
        return "Iced Blueberry";
      case "6":
        return "Iced Plum";
      case "7":
        return "Iced Mango";
      case "8":
        return "Iced Sala";
      case "9":
        return "Iced Limeade Sala";
    }
    return "";
  }

  void showHotCoffeeMenu() {
    printLine();
    print("1.Hot Caramel Latte");
    print("2.Hot Taiwanese Tea Cafe Latte");
    print("3.Hot Matcha Latte");
    print("4.Hot Kokuto Cafe Latte");
    print("5.Hot Thai Tea Cafe Latte");
    print("6.Hot Lychee Americano");
    print("7.Espresso");
    print("8.Double Espresso");
    print("9.Hot Americano");
    print("10.Hot Cafe Latte");
    print("11.Hot Cappuccino");
    print("12.Hot Mocha");
  }

  void showIcedCoffeeMenu() {
    printLine();
    print("1.Dirty");
    print("2.Iced Espresso");
    print("3.Iced Americano");
    print("4.Iced Cafe Latte");
    print("5.Iced Cappuccino");
    print("6.Iced Mocha");
    print("7.Iced Caramel Cafe Latte");
    print("8.Iced Matcha Cafe Latte");
    print("9.Iced Taiwanese Tea Cafe Latte");
    print("10.Iced Kokuto Cafe Latte");
    print("11.Iced Thai Tea Cafe Latte");
    print("12.Iced Lychee Americano");
    print("13.Iced Cannabis Americano");
  }

  void showHotTeaMenu() {
    printLine();
    print("Hot Tea:");
    print("1.Hot Chrysanthemum Tea");
    print("2.Hot Thai Milk Tea");
    print("3.Hot Taiwanese Tea");
    print("4.Hot Matcha Latte");
    print("5.Hot Black Tea");
    print("6.Hot Kokuto Tea");
    print("7.Hot Lime Tea");
    print("8.Hot Lychee Tea");
    print("9.Hot Strawberry Tea");
    print("10.Hot Blueberry Tea");
    print("11.Back");
  }

  void showIcedTeaMenu() {
    printLine();
    print("Iced Tea:");
    print("1.Iced Chrysanthemum Tea");
    print("2.Iced Thai Milk Tea");
    print("3.Iced Taiwanese Milk Tea");
    print("4.Iced Matcha Latte");
    print("5.Iced Tea");
    print("6.Iced Kokuto Tea");
    print("7.Iced Limeade Tea");
    print("8.Iced Lychee Tea");
    print("9.Iced Strawberry Tea");
    print("10.Iced Blueberry Tea");
    print("11.Back");
  }

  void showHotMilkMenu() {
    printLine();
    print("1.Hot Caramel Milk");
    print("2.Hot Kokuto Milk");
    print("3.Hot Cocoa");
    print("4.Hot Caramel Cocoa");
    print("5.Hot Milk");
  }

  void showIcedMilkMenu() {
    printLine();
    print("1.Iced Caramel Milk");
    print("2.Iced Kokuto Milk");
    print("3.Iced Cocoa");
    print("4.Iced Caramel Cocoa");
    print("5.Hot Pink Milk");
  }

  void showSodaMenu() {
    printLine();
    print("Soda:");
    print("1.Pepsi");
    print("2.Iced Limenade Soda");
    print("3.Iced Lychee Soda");
    print("4.Iced Strawberry Soda");
    print("5.Iced Cannabis Soda");
    print("6.Iced Plum Soda");
    print("7.Iced Ginger Soda");
    print("8.Iced Blueberry Soda");
    print("9.Iced Sala Soda");
    print("10.Iced Lime Sala Soda");
    print("11.Back");
  }

  void showProteinShakesMenu() {
    printLine();
    print("Protein Shakes:");
    print("1.Matcha Protein Shake");
    print("2.Chocolate Protein Shake");
    print("3.Strawberry Protein Shake");
    print("4.Espresso Protein Shake");
    print("5.Thai Tea Protein Shake");
    print("6.Brown Sugar Protein Shake");
    print("7.Taiwanese Tea Protein Shake");
    print("8.Caramel Protein Shake");
    print("9.Plain Protein Shake");
    print("10.Milk Shake");
    print("11.Back");
  }

  void showFruityDrinksMenu() {
    printLine();
    print("Fruity Drinks:");
    print("1.Hot Limeade");
    print("2.Iced Limeade");
    print("3.Iced Lychee");
    print("4.Iced Strawberry");
    print("5.Iced Blueberry");
    print("6.Iced Plum");
    print("7.Iced Mango");
    print("8.Iced Sala");
    print("9.Iced Limeade Sala");
    print("10.Back");
  }

  void printLine() {
    print("-------------------------------------------");
  }

  void showOption() {
    if (Option == "1") {
      printLine();
      print("Sweetness level");
      print("1.No sugar 2.Less Sweet 3.Just right 4.Sweet 5.Very Sweet");
      Sweetness = stdin.readLineSync()!;
      if (isCoffee) {
        printLine();
        print("Extra Topping");
        print("1 Shot of Espresso (+฿15)");
        print("1.Yes 2.No");
        ExtraTopping = stdin.readLineSync()!;
      } else {
        ExtraTopping = "2";
      }

      printLine();

      print("Do you want a Straw?");
      print("1.Yes 2.No");
      Straw = stdin.readLineSync()!;
      print("Do you want a Lid?");
      print("1.Yes 2.No");
      Lid = stdin.readLineSync()!;
      printLine();
    } else if (Option == "2") {
      printLine();
      print("Blended? (+฿5)");
      print("1.Yes 2.No");
      Blended = stdin.readLineSync()!;
      printLine();
      print("Sweetness level");
      print("1.No sugar 2.Less Sweet 3.Just right 4.Sweet 5.Very Sweet");
      Sweetness = stdin.readLineSync()!;

      if (isCoffee) {
        printLine();
        print("Extra Topping");
        print("1 Shot of Espresso (+฿15)");
        print("1.Yes 2.No");
        ExtraTopping = stdin.readLineSync()!;
      } else {
        ExtraTopping = "2";
      }

      printLine();
      print("Do you want a Straw?");
      print("1.Yes 2.No");
      Straw = stdin.readLineSync()!;
      print("Do you want a Lid?");
      print("1.Yes 2.No");
      Lid = stdin.readLineSync()!;
      printLine();
    }
  }

  void calcTotalPrice() {
    if (Blended == "1") {
      Price += 5;
    }
    if (ExtraTopping == "1") {
      Price += 15;
    }
  }

  void SweetnessToString() {
    switch (Sweetness) {
      case "1":
        Sweetness = "No Sugar ";
        break;
      case "2":
        Sweetness = "Less Sweet ";
        break;
      case "3":
        Sweetness = "";
        break;
      case "4":
        Sweetness = "Sweet ";
        break;
      case "5":
        Sweetness = "Very Sweet ";
        break;
      default:
        Sweetness = "";
    }
  }

  void printBill() {
    calcTotalPrice();
    print("Your order: $Sweetness$MenuName");
    print("Total price: ฿$Price");
    if (Straw == "1") {
      print("Straw [/]");
    } else {
      print("Straw [X]");
    }
    if (Lid == "1") {
      print("Lid [/]");
    } else {
      print("Lid [X]");
    }
    printLine();
  }

  void payBill() {
    print("Please insert your money:");
    int Money = int.parse(stdin.readLineSync()!);
    int Exchange = Money - Price;

    print("You inserted ฿$Money");
    print("Your exchange is ฿$Exchange");

    print("Enjoy your drink!");
  }
}

void main(List<String> arguments) {
  Taobinfly taobin = Taobinfly();
  taobin.showWelcome();
  taobin.inputCategory();
  taobin.process();
  taobin.printBill();
  taobin.payBill();
}
